//
//  LexicalTokentests.m
//  iOSCalc
//
//  Created by Michael Webster on 19/03/14.
//  Copyright (c) 2014 Mike Webster Ltd. All rights reserved.
//

#import "LexicalTokentests.h"
#import "LexicalToken.h"

@implementation LexicalTokentests
{
    LexicalToken* numToken;
    LexicalToken* unaryToken;
    LexicalToken* binaryToken;
    LexicalToken* lparenToken;
    LexicalToken* rparenToken;
}

- (void)setUp
{
    [super setUp];
    numToken = [LexicalToken createToken: NumberToken];
    unaryToken =
        [[LexicalToken alloc]initWithType:UnaryOpToken];
    binaryToken = [LexicalToken createToken:BinaryOpToken];
    lparenToken = [LexicalToken createToken:LParenToken];
    rparenToken = [LexicalToken createToken:RParenToken];
    
}

- (void)tearDown
{
    [super tearDown];
    numToken = nil;
    unaryToken = nil;
    binaryToken = nil;
    lparenToken = nil;
    rparenToken = nil;
}

- (void)testTokenType
{
    STAssertEquals
    (
        NumberToken,
        numToken.tokenType,
        @"Error: %d should be %d",
        numToken.tokenType,
        NumberToken
    );
    
    STAssertEquals
    (
        UnaryOpToken,
        unaryToken.tokenType,
        @"Error: %d should be %d",
        unaryToken.tokenType,
        UnaryOpToken
     );
    
    STAssertEquals
    (
        BinaryOpToken,
        binaryToken.tokenType,
        @"Error: %d should be %d",
        binaryToken.tokenType,
        BinaryOpToken
     );
    
    STAssertEquals
    (
        LParenToken,
        lparenToken.tokenType,
        @"Error: %d should be %d",
        lparenToken.tokenType,
        LParenToken
     );
    
    STAssertEquals
    (
        RParenToken,
        rparenToken.tokenType,
        @"Error: %d should be %d",
        rparenToken.tokenType,
        RParenToken
     );
}

-(void)testMethodFailures1
{
    BOOL didExcept = FALSE;
    NSDecimalNumber *num = nil;
    @try
    {
        num = [numToken evaluateNumber];
    }
    @catch (NSException* exception)
    {
        if ([[exception name] isEqualToString:
             [LexicalToken classException]])
        {
            didExcept = TRUE;
            NSLog(@"Exception for calling evaluateNumber");
            NSLog(@"Reaon is: %@", [exception reason]);
        }
        else
        {
            @throw exception;
        }
    }
    
    STAssertTrue(didExcept, @"Error: Expected an assert exception here.");
    
    didExcept = FALSE;
    @try
    {
        num = [unaryToken evaluateUnary:
                [NSDecimalNumber decimalNumberWithString:@"102"]];
    }
    @catch (NSException* exception)
    {
        if ([[exception name] isEqualToString:
             [LexicalToken classException]])
        {
            didExcept = TRUE;
            NSLog(@"Exception for calling evaluateUnary");
            NSLog(@"Reaon is: %@", [exception reason]);
        }
        else
        {
            @throw exception;
        }
    }
    
    STAssertTrue(didExcept, @"Error: Expected an assert exception here.");
    
    didExcept = FALSE;
    @try
    {
        num =
        [
            binaryToken
            evaluateBinary:
            [NSDecimalNumber decimalNumberWithString:@"@102"]
            andArg2:
            [NSDecimalNumber decimalNumberWithString:@"202"]
         ];
    }
    @catch (NSException* exception)
    {
        if ([[exception name] isEqualToString:
             [LexicalToken classException]])
        {
            didExcept = TRUE;
            NSLog(@"Exception for calling evaluateBinary");
            NSLog(@"Reaon is: %@", [exception reason]);
        }
        else
        {
            @throw exception;
        }
    }
    
    STAssertTrue(didExcept, @"Error: Expected an assert exception here.");

}

@end
