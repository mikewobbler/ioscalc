//
//  TestNumberTokenObject.m
//  iOSCalc
//
//  Created by Michael Webster on 19/03/14.
//  Copyright (c) 2014 Mike Webster Ltd. All rights reserved.
//

#import "TestNumberTokenObject.h"
#import "NumberTokenObject.h"

@implementation TestNumberTokenObject
- (void)setUp
{
    [super setUp];
    
    // Set-up code here.
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

- (void)testNumber
{
    NSInteger test_int = 123;
    NSString* test_string = [NSString stringWithFormat:@"%ld", test_int];
    NumberTokenObject *nto =
    [NumberTokenObject createNumberTokenWithNumString:test_string];
    
    STAssertEqualObjects
    (
        nto.tokenString,
        @"123",
        @"Error: %@ should equal %@",
        nto.tokenString,
        @"123"
    );
    
    [nto setTokenVal];
    
    STAssertEquals
    (
        [nto.tokenVal integerValue],
        test_int,
        @"Error: %@ should equal %@",
        [nto.tokenVal integerValue],
        test_int
     );

    
}

@end
