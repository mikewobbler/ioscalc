//
//  TestUnaryOpTokenObject.m
//  iOSCalc
//
//  Created by Michael Webster on 20/03/14.
//  Copyright (c) 2014 Mike Webster Ltd. All rights reserved.
//
#import <stdlib.h>
#import "TestUnaryOpTokenObject.h"
#import "UnaryOpTokenObject.h"


@implementation TestUnaryOpTokenObject
{
    UnaryOpTokenObject* bnot;
    UnaryOpTokenObject* uplus;
    UnaryOpTokenObject* uminus;
    UnaryOpTokenObject* sinOp;
    UnaryOpTokenObject* cosOp;
    UnaryOpTokenObject* tanOp;
    UnaryOpTokenObject* aSinOp;
    UnaryOpTokenObject* aCosOp;
    UnaryOpTokenObject* aTanOp;
    UnaryOpTokenObject* lnOp;
    UnaryOpTokenObject* logOp;
    UnaryOpTokenObject* lgOp;
    UnaryOpTokenObject* oneOnX;
    UnaryOpTokenObject* sqrtFn;
    UnaryOpTokenObject* factFn;
    UnaryOpTokenObject* sqrFn;
    UnaryOpTokenObject* tenToX;
    UnaryOpTokenObject* etoX;
    
    NSMutableArray* intArray;
    NSMutableArray* doubleArray;
}

- (void)setUp
{
    [super setUp];
    
    bnot = [UnaryOpTokenObject createWithUnaryOperator: BitwiseNot];
    uplus = [UnaryOpTokenObject createWithUnaryOperator: UnaryPlus];
    uminus = [UnaryOpTokenObject createWithUnaryOperator: UnaryMinus];
    sinOp = [UnaryOpTokenObject createWithUnaryOperator: SinFn];
    cosOp = [UnaryOpTokenObject createWithUnaryOperator: cosFn];
    tanOp = [UnaryOpTokenObject createWithUnaryOperator: tanFn];
    aSinOp = [UnaryOpTokenObject createWithUnaryOperator: ASinFn];
    aCosOp = [UnaryOpTokenObject createWithUnaryOperator: ACosFn];
    aTanOp = [UnaryOpTokenObject createWithUnaryOperator: ATanFn];
    lnOp = [UnaryOpTokenObject createWithUnaryOperator: LnFn];
    logOp = [UnaryOpTokenObject createWithUnaryOperator: LogFn];
    lgOp = [UnaryOpTokenObject createWithUnaryOperator: LgFn];
    oneOnX = [UnaryOpTokenObject createWithUnaryOperator: OneOnXFn];
    sqrtFn = [UnaryOpTokenObject createWithUnaryOperator: SqrtFn];
    factFn = [UnaryOpTokenObject createWithUnaryOperator: FactFn];
    sqrFn = [UnaryOpTokenObject createWithUnaryOperator: SquareFn];
    tenToX = [UnaryOpTokenObject createWithUnaryOperator: TenToXFn];
    etoX = [UnaryOpTokenObject createWithUnaryOperator: EToXFn];

    // Seed the random number generator.
    srandom(94356);
    intArray = [[NSMutableArray alloc] init];
    for (int i = 0; i < 20; i++)
    {
        long r = random();
        [intArray addObject:[NSDecimalNumber numberWithLong:r]];
    }
    
    doubleArray = [[NSMutableArray alloc] init];
    for (int i = 0; i < 20; i++)
    {
        long r = random();
        long x = [[intArray objectAtIndex:i] longValue];
        double result = x / r;
        [doubleArray addObject:[NSDecimalNumber numberWithDouble:result]];
    }
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

- (void)testLogs
{

    for (NSDecimalNumber* d in intArray)
    {
        NSDecimalNumber* result = [logOp evaluateUnary:d];
        NSLog(@"Log(%@) = %@", d, result);
    }
}

@end
