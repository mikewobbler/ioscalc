//
//  DebugMacros.h
//  MyCalc
//
//  Created by Michael Webster on 27/02/14.
//  Copyright (c) 2014 Mike Webster Ltd. All rights reserved.
//

#ifndef MyCalc_DebugMacros_h
#define MyCalc_DebugMacros_h

#ifdef DEBUG
#   define DLog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, \
            __LINE__, ##__VA_ARGS__)
#else
#   define DLog(...)
#endif

// ALog always displays output regardless of the DEBUG setting
#define ALog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__,   \
            __LINE__, ##__VA_ARGS__)

#endif /* MyCalc_DebugMacros_h */
