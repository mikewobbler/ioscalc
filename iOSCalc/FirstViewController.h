//
//  FirstViewController.h
//  iOSCalc
//
//  Created by Michael Webster on 16/03/14.
//  Copyright (c) 2014 Mike Webster Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FirstViewController : UIViewController
- (IBAction)number:(id)sender;
- (IBAction)addOp:(id)sender;
- (IBAction)mulOp:(id)sender;
- (IBAction)clear:(id)sender;
- (IBAction)equals:(id)sender;
- (IBAction)valueOp:(id)sender;
- (IBAction)functionOfOneArgument:(id)sender;
- (IBAction)functionOfTwoArguments:(id)sender;
@property (weak, nonatomic) IBOutlet UITextView *calcDisplay;

@end
