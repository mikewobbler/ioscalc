//
//  FirstViewController.m
//  iOSCalc
//
//  Created by Michael Webster on 16/03/14.
//  Copyright (c) 2014 Mike Webster Ltd. All rights reserved.
//

#import "FirstViewController.h"

@interface FirstViewController ()

@end

@implementation FirstViewController
@synthesize calcDisplay;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    calcDisplay.text = @"0.0";
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)number:(id)sender {
    NSString *title = [sender currentTitle];
    NSLog(@"Title is %@", title);
    calcDisplay.text = title;
}

- (IBAction)addOp:(id)sender {
}

- (IBAction)mulOp:(id)sender {
}

- (IBAction)clear:(id)sender {
}

- (IBAction)equals:(id)sender {
}

- (IBAction)valueOp:(id)sender {
}

- (IBAction)functionOfOneArgument:(id)sender {
}

- (IBAction)functionOfTwoArguments:(id)sender {
}
@end
