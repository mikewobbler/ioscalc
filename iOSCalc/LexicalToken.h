//
//  LexicalToken.h
//  iOSCalc
//
//  Created by Michael Webster on 18/03/14.
//  Copyright (c) 2014 Mike Webster Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MathDefs.h"

/**
 * Add appropriate functions with default implementations that raise an
 * exception. In the subclasses, those functions can use their saved block.
 */
typedef NSDecimalNumber* (^EvalNumber)(void);
typedef NSDecimalNumber* (^EvalUnary)(NSDecimalNumber*);
typedef NSDecimalNumber* (^EvalBinary)(NSDecimalNumber*, NSDecimalNumber*);

@interface LexicalToken : NSObject
@property TokenType tokenType;

-(id)initWithType: (TokenType)tt;

-(NSDecimalNumber*)evaluateNumber;
-(NSDecimalNumber*)evaluateUnary: (NSDecimalNumber*)arg;
-(NSDecimalNumber*)evaluateBinary: (NSDecimalNumber*)arg1
                          andArg2: (NSDecimalNumber*)arg2;

+(LexicalToken *)createToken: (TokenType)tt;
+(NSString*)classException;
@end
