//
//  LexicalToken.m
//  iOSCalc
//
//  Created by Michael Webster on 18/03/14.
//  Copyright (c) 2014 Mike Webster Ltd. All rights reserved.
//

#import "LexicalToken.h"

static NSString * const BaseClassFailException = @"BaseClassFailError";

@implementation LexicalToken

@synthesize tokenType;

-(id)initWithType: (TokenType) tt
{
    if ([self init])
    {
        tokenType = tt;
    }
    return self;
}

+(LexicalToken*)createToken: (TokenType)tt
{
    return [[LexicalToken alloc] initWithType: tt];
}

+(NSString*)classException
{
    return BaseClassFailException;
}

-(NSDecimalNumber*)evaluateNumber
{
    [NSException raise:BaseClassFailException
        format:@"%@ does not respond to the evaluateNumber message",
        NSStringFromClass([self class])
     ];
    return nil;
}

-(NSDecimalNumber*)evaluateUnary: (NSDecimalNumber*)arg
{
    [NSException raise:BaseClassFailException
                format:@"%@ does not respond to the evaluateUnary message",
     NSStringFromClass([self class])
     ];
    return nil;
}

-(NSDecimalNumber*)evaluateBinary:(NSDecimalNumber*)arg1
                          andArg2:(NSDecimalNumber*)arg2
{
    [NSException raise:BaseClassFailException
                format:@"%@ does not respond to the evaluateBinary message",
     NSStringFromClass([self class])
     ];
    return nil;
}

@end
