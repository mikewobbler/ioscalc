//
//  MathDefs.h
//  MyCalc
//
//  Created by Michael Webster on 21/02/14.
//  Copyright (c) 2014 Mike Webster Ltd. All rights reserved.
//

#ifndef MyCalc_MathDefs_h
#define MyCalc_MathDefs_h

typedef enum TokenType_t : NSInteger
{
    NumberToken,
    UnaryOpToken,
    BinaryOpToken,
    LParenToken,
    RParenToken
} TokenType;


typedef enum UnaryOperator_t : NSInteger
{
    BitwiseNot,         // ~
    LogicalNot,         // !
    UnaryPlus,          // +
    UnaryMinus,         // -
    SinFn,              // sin(x)
    cosFn,              // cos(x)
    tanFn,              // tan(x)
    ASinFn,             // arcsin(x)
    ACosFn,             // arccos(x)
    ATanFn,             // arctan(x)
    LnFn,              // ln(x)
    LogFn,             // log(x) (base 10)
    LgFn,              // lg(x)  (base 2)
    OneOnXFn,           // 1/x
    SqrtFn,             // sqrt(x)
    FactFn,             // x!
    SquareFn,           // x ^ 2
    TenToXFn,           // 10 ^ x
    EToXFn              // e ^ x
} UnaryOperator;

typedef enum BinaryOperator_t : NSInteger
{
    Add,                // x + y
    Subtract,           // x - y
    Multiply,           // x * y
    Divide,             // x / y
    Modulus,            // x % y
    BitwiseAnd,         // x & y
    BitwiseXor,         // x ^ y
    BitwiseOr,          // x | y
    LogicalAnd,         // x && y
    LogicalOr,          // x || y
    XToYFn,             // X to the power of y.
    XToOneOnYFn         // x ^ (1/y)
} BinaryOperator;

/**
 * Operator type predence enumeration for use by the calculator.
 */
typedef enum OperatorPrecedence : NSInteger
{
    IllegalLow,
    NumberType,     // A number
    MulOp,          // *, /, %
    AddOp,          // +, -
    ShiftOp,        // Bitwise left and right shift (<<, >>)
    BitwiseAndOp,   // Bitwise and (&)
    BitwiseXorOp,   // Bitwise xor (^)
    BitwiseOrOp,    // Bitwise or (|)
    LogicalAndOp,   // Logical and (&&)
    LogicalOrOp,    // Logical or (||)
    ParenOp,        // None of any use to calculator
    IllegalHigh
} OpType;

/**
 * The Calculator operators enumeration.
 */
typedef enum CalcOperators : NSInteger
{
    NoType = 0,
//    BitwiseNot,         // ~
//    LogicalNot,         // !
//    Multiply,           // *
//    Divide,             // /
//    Modulo,             // %
//    Add,                // +
//    Subtract,           // -
    BitwiseLeftShift,   // <<
    BitwiseRightShift,  // >>
    BitwiseAND,         // &
    BitwiseXOR,         // ^
    BitwiseOR,          // |
    LogicalAND,         // &&
    LogicalOR,          // ||
    LeftParen,          // (
    RightParen,         // )
    Number,             // A number
    Illegal
} CalcOp;

/**
 * The number Type enumeration.
 */
typedef enum numberType : NSInteger
{
    NotANumber,
    IntegerType,
    FloatingPointType
} CalcNumberType;

#endif
