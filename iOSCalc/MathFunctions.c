//
//  MathFunctions.c
//  iOSCalc
//
//  Created by Michael Webster on 20/03/14.
//  Copyright (c) 2014 Mike Webster Ltd. All rights reserved.
//

#include <stdio.h>
#include <math.h>
#include <assert.h>
#include "MathFunctions.h"

long long factorial(long long n)
{
    assert(n >= 0);
    
    if (n <= 0)
    {
        return 1;
    }
    else
    {
        return n * factorial(n - 1);
    }
}