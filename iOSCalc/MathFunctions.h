//
//  MathFunctions.h
//  iOSCalc
//
//  Created by Michael Webster on 20/03/14.
//  Copyright (c) 2014 Mike Webster Ltd. All rights reserved.
//

#ifndef iOSCalc_MathFunctions_h
#define iOSCalc_MathFunctions_h

long long factorial(long long n);

#endif
