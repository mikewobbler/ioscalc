//
//  NumberTokenObject.h
//  iOSCalc
//
//  Created by Michael Webster on 18/03/14.
//  Copyright (c) 2014 Mike Webster Ltd. All rights reserved.
//

#import "LexicalToken.h"

@interface NumberTokenObject : LexicalToken
@property (readonly)NSDecimalNumber* tokenVal;
@property NSMutableString* tokenString;
@property CalcNumberType numType;

/**
 * Extract the number enocoded in the tokenString string, an store it's
 * value as an NSNumber* in tokenVal.
 */
-(void)setTokenVal;

/**
 * Concatentate the supplied string to this classes tokenString, to add
 * digits to a number being read from the calculator screen.
 *
 * @param numPart    A string representation of the next bit of the number
 *                  to be added to tokenString.
 */
-(void)addNumberPart: (NSString*)numPart;

/**
 * Return the CalcNumberType value that identifies the type of number in
 * the string numString.
 *
 * @parem numString  A string representation of an integer or floating point
 *                   number in decimal representation.
 *
 * @return  The type of number contained in numString, or NotANumber if the
 *          string can't be recognised as a number.
 */
+(CalcNumberType)numberType:(NSString *)numString;

/**
 * Return true if tokenString is a number, otherwise, return false.
 *
 * @param tokenString   The string we are checking.
 *
 * @return True => tokenString contains a number, False otherwise.
 */
+(BOOL)stringIsNumber:(NSString *)tokenString;

-(id)initWithNumString: (NSString*)s;

+(NumberTokenObject*)createNumberTokenWithNumString:
    (NSString*) s;
@end
