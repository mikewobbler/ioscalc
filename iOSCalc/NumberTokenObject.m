//
//  NumberTokenObject.m
//  iOSCalc
//
//  Created by Michael Webster on 18/03/14.
//  Copyright (c) 2014 Mike Webster Ltd. All rights reserved.
//

#import "NumberTokenObject.h"
#undef DEBUG
#import "DebugMacros.h"
#define DEBUG

/**
 * A mappint from `CalcOp` values to `OpType` values.
 */
//static NSDictionary *calcOpToOpTypeMap;

static const unichar _plus       = '+';
static const unichar _minus      = '-';
static const unichar _shriek     = '!';
static const unichar _tilde      = '~';


static NSCharacterSet *decimalPointSet;

/**
 * Regular Expression for matching numbers, possibly prefixed by one of the
 * *+*, *!*, *~* or *-* unary operators.
 */
static NSString* numberPattern = @"^([+!~-])?(?:\\d+\\.)?\\d+$";

/**
 * `numRegex` is a private regular expression we create in the class, to
 * use for recognising numbers.
 */
static NSRegularExpression *numRegex;

@implementation NumberTokenObject
@synthesize tokenVal = _tokenVal;
@synthesize tokenString = _tokenString;
@synthesize numType = _numType;

/**
 * `initialize` initializes our private class variables `opTokenMap`,
 * `opTypeMap` and `numRegex`.
 *
 */
+(void)initialize
{
    // Setup the regular expression for recognising numbers.
    NSError *error = NULL;
    numRegex =
        [NSRegularExpression
            regularExpressionWithPattern:numberPattern
            options:0
            error:&error];
    
    if (error != nil)
    {
        DLog(@"Error creating numRegex RegularExpression.");
    }
    
    decimalPointSet =
    [NSCharacterSet characterSetWithCharactersInString: @"."];
    
}

-(void)addNumberPart:(NSString*)nPart
{
    if (self.tokenString == nil)
    {
        self.tokenString = [[NSMutableString alloc] init];
    }
    [self.tokenString appendString:nPart];
}

-(void)setTokenVal
{
    if ([NumberTokenObject stringIsNumber:self.tokenString])
    {
        self.numType =
            [NumberTokenObject numberType:self.tokenString];
        
        _tokenVal =
            [NSDecimalNumber decimalNumberWithString:self.tokenString];
    }
    else
    {
        [NSException raise:NSInvalidArgumentException
                    format:@"%@ Does not appear to be a valid number.",
                    self.tokenString];

    }
}

-(NSDecimalNumber*)evaluateNumber
{
    return _tokenVal;
}

-(id)initWithNumString:(NSString*)s
{
    if (self = [super initWithType:NumberToken])
    {
        self.tokenString = [[NSMutableString alloc] initWithString:s];
    }
    return self;
}

+(NumberTokenObject*)createNumberTokenWithNumString:
    (NSString*)s
{
    NumberTokenObject* nto =
    [[NumberTokenObject alloc] initWithNumString:s];
    return nto;
}

+(BOOL)stringIsNumber:(NSString *)tokenString
{
    NSUInteger matches =
    [
     numRegex numberOfMatchesInString:tokenString
     options:0
     range:NSMakeRange(0, [tokenString length])
     ];
    
    if (matches != 0)
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}


+(CalcNumberType)numberType:(NSString *)numString
{
    CalcNumberType nt = NotANumber;
    
    NSAssert1
    (
     [NumberTokenObject stringIsNumber: numString],
     @"numberType: %@ is not a valid Number.",
     numString
     );
    
    NSRange dotInString =
    [numString rangeOfCharacterFromSet:decimalPointSet];
    
    if (dotInString.location == NSNotFound)
    {
        // The number is an integer.
        nt = IntegerType;
    }
    else
    {
        if
            (
             ([numString characterAtIndex:0] == _shriek)
             ||
             ([numString characterAtIndex:0] == _tilde)
             )
        {
            [NSException raise:NSInvalidArgumentException
                        format:@"numberType: Type of %@ is invalid - %@",
             numString,
             @"can't apply ! or ~ to a floating point number"
             ];
        }
        nt = FloatingPointType;
    }
    return nt;
}

@end
