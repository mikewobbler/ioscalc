//
//  TreeNode.h
//  MyCalc
//
//  Created by Michael Webster on 27/02/14.
//  Copyright (c) 2014 Mike Webster Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#include "LexicalToken.h"

/**
 * The `TreeNode` class is used to represent nodes in a parse tree for the
 * calculator. A `TreeNode` instance has the following properties:
 * - `nodeToken`    The `LexicalToken` that identifies the type and/or value
 *                  of this node.
 * - leftTree       A `TreeNode`.
 * - rightTree      Another `TreeNode`
 *
 * `TreeNode`'s are used by the `ArithmeticParser` class to construct parse
 * trees for performing arithmetic.
 */
@interface TreeNode : NSObject

/**
 * The 'LexicalToken` contained in this node.
 */
@property   LexicalToken*   nodeToken;

/**
 * `leftTree` is a subtree hanging off the left side of this `TreeNode`.
 */
@property   TreeNode*       leftTree;

/**
 * `rightTree` is a subtree hanging off the right hand sized of this
 * `TreeNode`.
 */
@property   TreeNode*       rightTree;

/**
 * Initialize and return this `TreeNode` by storing the supplied
 * `LexicalToken`.
 *
 * @param token     The token for this `TreeNode`.
 *
 * @return      The initialised `TreeNode`.
 */
-(id)initWithToken: (LexicalToken *)token;

/**
 * Add the `TreeNode *node` to the right subtree of this `TreeNode` in such
 * a way that correct precedence for the `TreeNode`'s calculated value is
 * maintained.
 *
 * @param node      The `TreeNode` to be added to the right subtree.
 */
-(void)addRightTree: (TreeNode *)node;

/**
 * `setLeft` simply sets leftTree to be the supplied node.
 *
 * @param node  The node that leftNode is to be assigned to.
 */
-(void)setLeft: (TreeNode *)node;

/**
 * `setRightTree` simply sets rightTree to be the supplied node.
 *
 * @param node  The node that rightNode is to be assigned to.
 */
-(void)setRight: (TreeNode *)node;

/**
 * Return the value of the tree rooted at this Node by evaluating the
 * left and right subtrees, and applying this node's operator to the
 * two values obtained.
 *
 * @return The value obtained by evaluateing the tree rooted at this node.
 */
-(NSNumber*)evaluateNode;

/**
 * Return the precedence value (`opType`) of the `LexicalToken` contained in
 * this node.
 */
-(NSInteger)getPrecedence;

/**
 * `walk` the tree by at each node, walking the left node first, looking
 * at this node, then walking the right node. This is an inOrder walk.
 *
 * FIXME: Think of passing a block in here so we can do something as we
 * walk the tree.
 */
-(void)walkTreePrint;

/**
 * Given a left and right operand, `calculateNodeValue` returns the value
 * obtained by applying the operator encoded in `applyOp` to the supplied
 * left and right operands.
 *
 * @param applyOp       The operator to be applied to the two operands.
 * @param leftOperand   The left operand in the application of `applyOp`.
 * @param rightOperand  The right operand in the application of `applyOp`.
 *
 * @return  The value that results from applying `applyOp` to `leftOperand`
 *          and `rightOperand`.
 */
+(NSNumber*)calculateNodeValue: (LexicalToken*)applyOp
                        toLeft: (NSNumber *)leftOperand
                      andRight:(NSNumber *)rightOperand;

@end
