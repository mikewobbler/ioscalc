//
//  TreeNode.m
//  MyCalc
//
//  Created by Michael Webster on 27/02/14.
//  Copyright (c) 2014 Mike Webster Ltd. All rights reserved.
//

#import "TreeNode.h"

@implementation TreeNode

@synthesize nodeToken, leftTree, rightTree;

-(id)initWithToken:(LexicalToken *)token
{
    if (self = [super init])
    {
        nodeToken = token;
    }
    return self;
}

-(void)addRightTree:(TreeNode *)node
{
    if (nil == rightTree)
    {
        [self setRight: node];
    }
    else if ([[node nodeToken]isNumber])
    {
        [[self rightTree]addRightTree: node];
    }
    else if([node getPrecedence] < [[self rightTree]getPrecedence])
    {
        [[self rightTree]addRightTree:node];
    }
    else
    {
        [node setLeft: [self rightTree]];
        [self setRight: node];
    }
}

-(void)setLeft:(TreeNode *)node
{
    leftTree = node;
}

-(void)setRight:(TreeNode *)node
{
    rightTree = node;
}

-(NSNumber*)evaluateNode
{
    if ([nodeToken isInteger])
    {
        return [NSNumber numberWithLongLong:
                [nodeToken asLongLong]];
    }
    else if ([nodeToken isDouble])
    {
        return [NSNumber numberWithDouble:
                [nodeToken asDouble]];
    }
    else
    {
        NSAssert
        (
            leftTree != NULL && rightTree != NULL,
            @"Error: leftTree(%@) and rightTree(%@) cannot be NULL.",
            leftTree,
            rightTree
        );
        
        // Evaluate left and right subtrees.
        NSNumber *lte = [leftTree evaluateNode];
        NSNumber *rte = [rightTree evaluateNode];
        return [TreeNode
                calculateNodeValue: nodeToken
                toLeft: lte
                andRight:rte];
    }
}
  
-(NSInteger)getPrecedence
{
    return nodeToken.opType;
}

-(void)walkTreePrint
{
    if (leftTree != nil)
    {
        [leftTree walkTreePrint];
    }
    [nodeToken print];
    if (rightTree != nil)
    {
        [rightTree walkTreePrint];
    }
}

+(NSNumber *)calculateNodeValue:
    (LexicalToken *)applyOp
    toLeft:(NSNumber *)leftOperand
    andRight:(NSNumber *)rightOperand
{
    NSNumber* value = nil;
    double dVal = 0;
    long long lval = 0;
    BOOL leftDouble = FALSE;
    BOOL rightDouble = FALSE;
    BOOL resultIsDouble = FALSE;
    
    if (strcmp([leftOperand objCType], "d") == 0)
    {
        leftDouble = TRUE;
    }
    
    if (strcmp([rightOperand objCType], "d") == 0)
    {
        rightDouble = TRUE;
    }

    resultIsDouble = leftDouble || rightDouble;

    switch (applyOp.calcOp)
    {
        case Multiply:           // '*'
            if (resultIsDouble)
            {
                dVal =
                (leftDouble ? [leftOperand doubleValue] :
                 [leftOperand longLongValue])
                *
                (rightDouble ? [rightOperand doubleValue] :
                 [rightOperand longLongValue]);
                value = [NSNumber numberWithDouble:dVal];
            }
            else
            {
                lval = [leftOperand longLongValue] *
                    [rightOperand longLongValue];
                value = [NSNumber numberWithLongLong:lval];
            }
            break;
        case Divide:             // /
            // Note: For divide I'm always going to return a double.
            dVal =
            (leftDouble ? [leftOperand doubleValue] :
             [leftOperand longLongValue])
            /
            (rightDouble ? [rightOperand doubleValue] :
             [rightOperand longLongValue]);
            value = [NSNumber numberWithDouble:dVal];
            break;
        case Modulo:             // %
            // Note: For modulo - both operands must be integers.
            NSAssert
            (
                !leftDouble && !rightDouble,
                @"Error: Modulus doesn't apply to integral values (%@, %@)",
                leftOperand,
                rightOperand
            );
            lval = [leftOperand longLongValue] % [rightOperand longLongValue];
            value = [NSNumber numberWithLongLong:lval];
            break;
        case Add:                // +
            if (resultIsDouble)
            {
                dVal =
                (leftDouble ? [leftOperand doubleValue] :
                 [leftOperand longLongValue])
                +
                (rightDouble ? [rightOperand doubleValue] :
                 [rightOperand longLongValue]);
                value = [NSNumber numberWithDouble:dVal];
            }
            else
            {
                lval = [leftOperand longLongValue] +
                [rightOperand longLongValue];
                value = [NSNumber numberWithLongLong:lval];
            }
            break;
        case Subtract:           // -
            if (resultIsDouble)
            {
                dVal =
                (leftDouble ? [leftOperand doubleValue] :
                 [leftOperand longLongValue])
                    -
                (rightDouble ? [rightOperand doubleValue] :
                 [rightOperand longLongValue]);
                value = [NSNumber numberWithDouble:dVal];
            }
            else
            {
                lval = [leftOperand longLongValue] -
                    [rightOperand longLongValue];
                value = [NSNumber numberWithLongLong:lval];
            }

            break;
        case BitwiseLeftShift:   // <<
            // Note: For left shift - both operands must be integers.
            NSAssert
            (
                !leftDouble && !rightDouble,
                @"Error: L Shift doesn't apply to integral values (%@, %@)",
                leftOperand,
                rightOperand
             );
            lval = [leftOperand longLongValue] << [rightOperand longLongValue];
            value = [NSNumber numberWithLongLong:lval];
            break;
        case BitwiseRightShift:  // >>
            // Note: For right shift - both operands must be integers.
            NSAssert
            (
                !leftDouble && !rightDouble,
                @"Error: L Shift doesn't apply to integral values (%@, %@)",
                leftOperand,
                rightOperand
            );
            lval = [leftOperand longLongValue] >> [rightOperand longLongValue];
            value = [NSNumber numberWithLongLong:lval];
            break;
        case BitwiseAND:         // &
            // Note: For & - both operands must be integers.
            NSAssert
            (
             !leftDouble && !rightDouble,
             @"Error: L Shift doesn't apply to integral values (%@, %@)",
             leftOperand,
             rightOperand
             );
            lval = [leftOperand longLongValue] & [rightOperand longLongValue];
            value = [NSNumber numberWithLongLong:lval];
            break;
        case BitwiseXOR:         // ^
            // Note: For XOR - both operands must be integers.
            NSAssert
            (
             !leftDouble && !rightDouble,
             @"Error: L Shift doesn't apply to integral values (%@, %@)",
             leftOperand,
             rightOperand
             );
            lval = [leftOperand longLongValue] ^ [rightOperand longLongValue];
            value = [NSNumber numberWithLongLong:lval];
            break;
        case BitwiseOR:          // |
            // Note: For | - both operands must be integers.
            NSAssert
            (
             !leftDouble && !rightDouble,
             @"Error: L Shift doesn't apply to integral values (%@, %@)",
             leftOperand,
             rightOperand
             );
            lval = [leftOperand longLongValue] | [rightOperand longLongValue];
            value = [NSNumber numberWithLongLong:lval];
            break;
        case LogicalAND:         // &&
            // Note: For && - both operands must be integers.
            NSAssert
            (
             !leftDouble && !rightDouble,
             @"Error: L Shift doesn't apply to integral values (%@, %@)",
             leftOperand,
             rightOperand
             );
            lval = [leftOperand longLongValue] && [rightOperand longLongValue];
            value = [NSNumber numberWithLongLong:lval];
            break;
        case LogicalOR:          // ||
            // Note: For || - both operands must be integers.
            NSAssert
            (
             !leftDouble && !rightDouble,
             @"Error: L Shift doesn't apply to integral values (%@, %@)",
             leftOperand,
             rightOperand
             );
            lval = [leftOperand longLongValue] || [rightOperand longLongValue];
            value = [NSNumber numberWithLongLong:lval];
            break;
/*        case LeftParen:          // (
            break;
        case RightParen:         // )
            break;*/
        default:
            break;
    }
    return value;
}

@end
