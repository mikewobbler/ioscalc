//
//  UnaryOpTokenObject.h
//  iOSCalc
//
//  Created by Michael Webster on 19/03/14.
//  Copyright (c) 2014 Mike Webster Ltd. All rights reserved.
//

#import "LexicalToken.h"

@interface UnaryOpTokenObject : LexicalToken
@property (readonly) EvalUnary unaryEvaluator;
@property (readonly) UnaryOperator unOp;

-(NSDecimalNumber*)evaluateUnary: (NSDecimalNumber*)arg;
-(id)initWithUnaryOperator: (UnaryOperator)uo;
+(UnaryOpTokenObject*)createWithUnaryOperator: (UnaryOperator)uo;
@end
