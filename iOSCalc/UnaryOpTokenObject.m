//
//  UnaryOpTokenObject.m
//  iOSCalc
//
//  Created by Michael Webster on 19/03/14.
//  Copyright (c) 2014 Mike Webster Ltd. All rights reserved.
//

#import "UnaryOpTokenObject.h"
#import "math.h"
#import "MathFunctions.h"

@implementation UnaryOpTokenObject
@synthesize unaryEvaluator = _unaryEvaluator;
@synthesize unOp = _unOp;

-(NSDecimalNumber*)evaluateUnary:(NSDecimalNumber *)arg
{
    return self.unaryEvaluator(arg);
}

-(id)initWithUnaryOperator:(UnaryOperator)uo
{
    if (self = [super initWithType:UnaryOpToken])
    {
        _unOp = uo;
        switch (uo)
        {
            case BitwiseNot:         // ~
                _unaryEvaluator = ^(NSDecimalNumber* arg)
                {
                    unsigned long long x = [arg unsignedLongLongValue];
                    return (NSDecimalNumber*)[NSDecimalNumber numberWithUnsignedLongLong:x];
                };
                break;
            case UnaryPlus:          // +
                _unaryEvaluator = ^(NSDecimalNumber* arg)
                {
                    return arg;
                };
                break;
            case UnaryMinus:         // -
                _unaryEvaluator = ^(NSDecimalNumber* arg)
                {
                    NSDecimalNumber* neg1 = [NSDecimalNumber decimalNumberWithString:@"-1"];
                    return [arg decimalNumberByMultiplyingBy:neg1];
                };
                break;
            case SinFn:              // sin(x)
                _unaryEvaluator = ^(NSDecimalNumber* arg)
                {
                    double x = [arg doubleValue];
                    return (NSDecimalNumber*)[NSDecimalNumber numberWithDouble: sin(x)];
                };
                break;
            case cosFn:              // cos(x)
                _unaryEvaluator = ^(NSDecimalNumber* arg)
                {
                    double x = [arg doubleValue];
                    return (NSDecimalNumber*)[NSDecimalNumber numberWithDouble: cos(x)];
                };
                break;
            case tanFn:              // tan(x)
                _unaryEvaluator = ^(NSDecimalNumber* arg)
                {
                    double x = [arg doubleValue];
                    return (NSDecimalNumber*)[NSDecimalNumber numberWithDouble: tan(x)];
                };
                break;
            case ASinFn:             // arcsin(x)
                _unaryEvaluator = ^(NSDecimalNumber* arg)
                {
                    double x = [arg doubleValue];
                    return (NSDecimalNumber*)[NSDecimalNumber numberWithDouble: asin(x)];
                };
                break;
            case ACosFn:             // arccos(x)
                _unaryEvaluator = ^(NSDecimalNumber* arg)
                {
                    double x = [arg doubleValue];
                    return (NSDecimalNumber*)[NSDecimalNumber numberWithDouble: acos(x)];
                };
                break;
            case ATanFn:             // arctan(x)
                _unaryEvaluator = ^(NSDecimalNumber* arg)
                {
                    double x = [arg doubleValue];
                    return (NSDecimalNumber*)[NSDecimalNumber numberWithDouble: atan(x)];
                };
                break;
            case LnFn:              // ln(x)
                _unaryEvaluator = ^(NSDecimalNumber* arg)
                {
                    double x = [arg doubleValue];
                    return (NSDecimalNumber*)[NSDecimalNumber numberWithDouble: log(x)];
                };
                break;
            case LogFn:             // log(x) (base 10)
                _unaryEvaluator = ^(NSDecimalNumber* arg)
                {
                    double x = [arg doubleValue];
                    return (NSDecimalNumber*)[NSDecimalNumber numberWithDouble: log10(x)];
                };
                break;
            case LgFn:              // lg(x)  (base 2)
                _unaryEvaluator = ^(NSDecimalNumber* arg)
                {
                    double x = [arg doubleValue];
                    return (NSDecimalNumber*)[NSDecimalNumber numberWithDouble: log2(x)];
                };
                break;
            case OneOnXFn:           // 1/x
                _unaryEvaluator = ^(NSDecimalNumber* arg)
                {
                    double x = [arg doubleValue];
                    if (0.0 == x)
                    {
                        [NSException raise:NSInvalidArgumentException
                                    format:@"Argument cannot be 0"];
                    }
                    return (NSDecimalNumber*)[NSDecimalNumber numberWithDouble: 1/x];
                };
                break;
            case SqrtFn:             // sqrt(x)
                _unaryEvaluator = ^(NSDecimalNumber* arg)
                {
                    double x = [arg doubleValue];
                    return (NSDecimalNumber*)[NSDecimalNumber numberWithDouble: sqrt(x)];
                };
                break;
            case FactFn:             // x!
                _unaryEvaluator = ^(NSDecimalNumber* arg)
                {
                    double x = [arg longLongValue];
                    return (NSDecimalNumber*)[NSDecimalNumber numberWithLongLong: factorial(x)];
                };
                break;
            case SquareFn:           // x ^ 2
                _unaryEvaluator = ^(NSDecimalNumber* arg)
                {
                    return [arg decimalNumberByRaisingToPower: 2];
                };
                break;
            case TenToXFn:           // 10 ^ x
                _unaryEvaluator = ^(NSDecimalNumber* arg)
                {
                    double x = [arg doubleValue];
                    return (NSDecimalNumber*)[NSDecimalNumber numberWithDouble: pow(10.0,x)];
                };
                break;
            case EToXFn:             // e ^ x
                _unaryEvaluator = ^(NSDecimalNumber* arg)
                {
                    double x = [arg doubleValue];
                    return (NSDecimalNumber*)[NSDecimalNumber numberWithDouble: exp(x)];
                };
                break;
            default:
                NSLog(@"Unrecognised UnaryOperator %ld passed in.", uo);
                break;
        }
    }
    return self;
}

+(UnaryOpTokenObject*)createWithUnaryOperator: (UnaryOperator)uo
{
    return [[UnaryOpTokenObject alloc] initWithUnaryOperator:uo];
}
@end
