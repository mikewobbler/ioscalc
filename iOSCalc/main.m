//
//  main.m
//  iOSCalc
//
//  Created by Michael Webster on 16/03/14.
//  Copyright (c) 2014 Mike Webster Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
