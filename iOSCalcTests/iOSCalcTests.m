//
//  iOSCalcTests.m
//  iOSCalcTests
//
//  Created by Michael Webster on 16/03/14.
//  Copyright (c) 2014 Mike Webster Ltd. All rights reserved.
//

#import "iOSCalcTests.h"

@implementation iOSCalcTests

- (void)setUp
{
    [super setUp];
    
    // Set-up code here.
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

- (void)testExample
{
    STFail(@"Unit tests are not implemented yet in iOSCalcTests");
}

@end
